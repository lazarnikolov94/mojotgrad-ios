//
//  PhotoAlbumCollectionViewCell.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 7/31/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import UIKit

class PhotoAlbumCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    
    func setCellImage(image: UIImage) {
        self.image.image = image
    }
}

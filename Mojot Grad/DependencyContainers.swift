//
//  DependencyContainers.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 7/30/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import Foundation
import Dip

func configureContainer(dip: DependencyContainer) {
    
    // Services
    dip.register(.Singleton) { ServiceFactoryImp() as ServiceFactory }
    dip.register(.Singleton) { StorageServiceImp() as StorageService }
    
    // View Models
    dip.register() { PhotoAlbumViewModel(Factory: try! dip.resolve() as ServiceFactory) as PhotoAlbumViewModel }
    dip.register() { MapViewModel(Factory: try! dip.resolve() as ServiceFactory) as MapViewModel }
}
//
//  PhotoViewerViewController.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 8/3/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import UIKit
import Photos

class PhotoViewerViewController: UIViewController {

    @IBOutlet weak var photoView: UIImageView!
    
    var asset: PHAsset!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: self.view.frame.size, contentMode: .AspectFill, options: nil, resultHandler: { result, info in
            if let image = result {
                self.photoView.image = image
            }
        })
    }
    
    @IBAction func sharePhoto(sender: AnyObject) {
        let photoToShare = [self.photoView.image!]
        let activityViewController = UIActivityViewController(activityItems: photoToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = sender as? UIView
        
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func deletePhoto() {
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({
            PHAssetChangeRequest.deleteAssets([self.asset])
            }, completionHandler: { success, error in
                if success {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.navigationController!.popViewControllerAnimated(true)
                    }
                }
        })
    }

}

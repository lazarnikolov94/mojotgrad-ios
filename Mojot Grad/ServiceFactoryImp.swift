//
//  ServiceFactoryImp.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 7/30/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import UIKit

class ServiceFactoryImp: ServiceFactory {
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var Storage: StorageService {
        return try! appDelegate.container.resolve() as StorageService
    }
    
}
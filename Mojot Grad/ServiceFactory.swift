//
//  ServiceFactory.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 7/30/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import Foundation

protocol ServiceFactory {
    
    var Storage: StorageService { get }
    
}
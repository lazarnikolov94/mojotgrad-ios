//
//  PhotoAlbumViewModel.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 7/31/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import UIKit
import Photos

class PhotoAlbumViewModel {

    var Factory: ServiceFactory!
    var assetCollection: PHAssetCollection!
    var photosAsset: PHFetchResult!
    let albumName = "Mojot Grad"
    
    init(Factory: ServiceFactory) {
        self.Factory = Factory
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", self.albumName)
        let collection = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOptions)
        if let first_Obj:AnyObject = collection.firstObject{
            //found the album
            self.assetCollection = first_Obj as! PHAssetCollection
            self.photosAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollection, options: nil)
        } else {
            var albumPlaceholder:PHObjectPlaceholder!
            PHPhotoLibrary.sharedPhotoLibrary().performChanges({
                    let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollectionWithTitle(self.albumName)
                    albumPlaceholder = request.placeholderForCreatedAssetCollection
                },
                completionHandler: {(success:Bool, error:NSError?)in
                    if(success){
                        print("Successfully created folder")
                        let collection = PHAssetCollection.fetchAssetCollectionsWithLocalIdentifiers([albumPlaceholder.localIdentifier], options: nil)
                        self.assetCollection = collection.firstObject as! PHAssetCollection
                        self.photosAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollection, options: nil)
                    } else {
                        print("Error creating folder")
                    }
            })
        }
        
    }
    
    func SaveImage(image: UIImage, completionHandler: (() -> Void)) {
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({
            let createAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(image)
            let assetPlaceholder = createAssetRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(forAssetCollection: self.assetCollection, assets: self.photosAsset)
            albumChangeRequest?.addAssets([assetPlaceholder!])
            }, completionHandler: { success, error in
                completionHandler()
        })
    }
    
    func GetAlbumData() -> [UIImage] {
        self.photosAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollection, options: nil)
        var imagesArray: [UIImage] = []
        for i in 0 ..< self.photosAsset.count {
            let asset: PHAsset = self.photosAsset[i] as! PHAsset
            PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: CGSize(width: 300, height: 420), contentMode: .AspectFill, options: nil, resultHandler: { result, info in
                if let image = result {
                    imagesArray.append(image)
                }
            })
        }
        return imagesArray
    }
    
    func GetPhotosAsset() -> [PHAsset] {
        var assets: [PHAsset] = []
        
        self.photosAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollection, options: nil)
        for i in 0 ..< self.photosAsset.count {
            let asset : PHAsset = self.photosAsset[i] as! PHAsset
            assets.append(asset)
        }
        
        return assets
    }
    
    func GetAssetAtIndex(index: Int) -> PHAsset {
        self.photosAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollection, options: nil)
        return self.photosAsset[index] as! PHAsset
    }
    
}
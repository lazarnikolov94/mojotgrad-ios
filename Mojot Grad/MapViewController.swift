//
//  MapViewController.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 7/31/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let initialLocation = CLLocationCoordinate2D(latitude: 42.00207, longitude: 21.4123764)
    let regionRadius: CLLocationDistance = 3000
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let locationManager = CLLocationManager()
    
    var Model: MapViewModel!
    
    var longPressGestureRecognizer: UILongPressGestureRecognizer!
    var pinNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        
        Model = try! appDelegate.container.resolve() as MapViewModel
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(initialLocation, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(MapViewController.addAnnotation(_:)))
        longPressGestureRecognizer.minimumPressDuration = 2.0
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let coord = locations.last?.coordinate
        print("Longitude: \(coord?.longitude), latitude: \(coord?.latitude)")
    }
    
    @IBAction func enableMapGestureRecognizer() {
        mapView.addGestureRecognizer(longPressGestureRecognizer)
        self.title = "Притиснете 2 секунди"
        self.navigationController?.title = "Притиснете 2 секунди"
    }
    
    func addAnnotation(gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state == .Began {
            let touchPoint = gestureRecognizer.locationInView(mapView)
            let newCoordinates = mapView.convertPoint(touchPoint, toCoordinateFromView: mapView)
            
            let newAnnotation = MKPointAnnotation()
            newAnnotation.coordinate = newCoordinates
            
            let pinAlert = UIAlertController(title: "Име на пинот", message: "Внесете име на пинот", preferredStyle: .Alert)
            pinAlert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
                textField.placeholder = "Име на пинот"
                self.pinNameTextField = textField
            })
            pinAlert.addAction(UIAlertAction(title: "ОК", style: .Default, handler: { action in
                newAnnotation.title = self.pinNameTextField.text
                self.mapView.addAnnotation(newAnnotation)
                self.mapView.removeGestureRecognizer(self.longPressGestureRecognizer)
                self.title = "Мапа"
            }))
            
            self.presentViewController(pinAlert, animated: true, completion: nil)
        }
    }
}

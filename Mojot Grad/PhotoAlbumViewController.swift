//
//  PhotoAlbumViewController.swift
//  Mojot Grad
//
//  Created by Lazar Nikolov on 7/31/16.
//  Copyright © 2016 Lazar Nikolov. All rights reserved.
//

import UIKit
import Photos

class PhotoAlbumViewController:
UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var photoCollection: UICollectionView!
    @IBOutlet weak var noDataView: UIView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var Model: PhotoAlbumViewModel!
    var imagePicker: UIImagePickerController!
    
    var albumImages: [UIImage] = []
    
    var selectedAsset: PHAsset!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.noDataView.hidden = true
        
        Model = try! appDelegate.container.resolve() as PhotoAlbumViewModel
        
        photoCollection.delegate = self
        photoCollection.dataSource = self
        
        if(UIImagePickerController.isSourceTypeAvailable(.Camera)) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .Camera
            imagePicker.allowsEditing = false
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        updateCollection()
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.albumImages.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.selectedAsset = Model.GetAssetAtIndex(indexPath.row)
        self.performSegueWithIdentifier("showLargePhoto", sender: self)
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: PhotoAlbumCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("photoAlbumCollectionViewCell", forIndexPath: indexPath) as! PhotoAlbumCollectionViewCell
        
        cell.setCellImage(self.albumImages[indexPath.row])
        
        return cell
    }
    
    @IBAction func TakePhoto() {
        if imagePicker != nil {
            self.presentViewController(imagePicker, animated: true, completion: nil)
        } else {
            // TODO: Show alert that the device doesn't support Camera
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if let originalImage = info[UIImagePickerControllerOriginalImage] {
            self.Model.SaveImage(originalImage as! UIImage, completionHandler: {
                self.updateCollection()
            })
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(ac, animated: true, completion: nil)
        }
    }
    
    func updateCollection() {
        dispatch_async(dispatch_get_main_queue()) {
            self.albumImages = self.Model.GetAlbumData()
            
            if self.albumImages.count == 0 {
                self.noDataView.hidden = false
                self.photoCollection.hidden = true
            } else {
                self.noDataView.hidden = true
                self.photoCollection.hidden = false
            }
            self.photoCollection.reloadData()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showLargePhoto" {
            let destinationViewController: PhotoViewerViewController = segue.destinationViewController as! PhotoViewerViewController
            destinationViewController.asset = self.selectedAsset
        }
    }
}
